package ch04operators;

public class BitwiseOperators {

    /**
     * Java defines several bitwise operator that can be applied to the integer types
     *
     * ~    Bitwise unary NOT
     * &    Bitwise AND
     * |    Bitwise OR
     * ^    Bitwise exclusive OR
     * >>   Shift right
     * >>>  Shift right zero fill
     * <<   Shift left
     * &=   Bitwise AND assignment
     * |=   Bitwise OR assignment
     * ^=   Bitwise exclusive OR assignment
     * >>=  Shift right assignment
     * >>>= Shift right zero fill assignment
     * <<=  Shift left assignment
     */

    /**
     * The Bitwise Logical Operators
     *
     * - To represent a negative number we first invert each bit, then add 1
     *
     * ~ NOT
     * 00101010 => 11010101
     *
     * & AND
     *  00101010 42
     * &00001111 45
     * _____________
     *  00001010 10
     *
     * | OR
     *  00101010 42
     * |00001111 15
     * ____________
     * 00101111 47
     *
     * ^ XOR
     * Combines bits such that if exactly one operand is 1, then the result is 1
     * Otherwise the result is zero
     *  00101010 42
     * ^00001111 15
     * ____________
     *  00100101 37
     */
    public static void main(String[] args) {
        System.out.println("The Bitwise Logical Operators");
        System.out.println(42 & 15);

        String binary[] = {
                "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
                "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"
        };
        int a = 3; // 0111
        int b = 6; // 0110
        int c = a | b;
        int d = a & b;
        int e = a ^ b;
        int f = (~a & b) | (a & ~b);
        int g = ~a & 0x0f;

        System.out.println("        a = " + binary[a]);
        System.out.println("        b = " + binary[b]);
        System.out.println("      a|b = " + binary[c]);
        System.out.println("      a&b = " + binary[d]);
        System.out.println("      a^b = " + binary[e]);
        System.out.println("~a&b|a&~b = " + binary[f]);
        System.out.println("       ~a = " + binary[g]);

        System.out.println("\nThe Left Shift");
        byte aa = 64, bb;
        int i;

        i = aa << 2;
        bb = (byte) (aa << 2);

        System.out.println("Original value of a: " + aa);
        System.out.println("i and b: " + i + " " + bb);

        System.out.println("\nMultByTwo");

        int num = 0xFFFFFFE;
        System.out.println(num);
        System.out.println();
        System.out.println(num * 2);
        System.out.println((num * 2) * 2);
        System.out.println(((num * 2) * 2) * 2);
        System.out.println((((num * 2) * 2) * 2) * 2);

        System.out.println();

        for (i = 0; i < 4; i++) {
            num = num << 1;
            System.out.println(num);
        }

        /**
         * The RightShift
         */
        System.out.println("\nThe Right Shift");

        int ra = 32;
        ra = ra >> ra; // a now contains 8
        System.out.println("ra: " + ra);
        /**
         * 00100011 35
         * >> 2
         * 00001000 8
         */

        ra = -8;
        ra = ra >> 1;
        System.out.println("ra: " + ra);

        /**
         * HexByte
         */
        System.out.println("\nHex Byte");
        char hex[] = {
                '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
        };

        byte b1 = (byte) 0xf1;
        System.out.println((b1 >> 4) & 0x0f);
        System.out.println(b1 & 0x0f);
        System.out.println("b = 0x" + hex[(b1 >> 4) & 0x0f] + hex[b1 & 0x0f]);

        /**
         * The Unsigned Right Shift
         */
        System.out.println("\nThe Unsigned Right Shift");
        byte b2 = (byte) 0xf1;
        byte c2 = (byte) (b2 >> 4);
        byte d2 = (byte) (b2 >>> 4);

        System.out.println("b2: " + b2);
        System.out.println("c2: " + c2);
        System.out.println("d2: " + d2);

        /**
         * Bitwise Operator Compound Assignments
         *
         * a = a >> 4;
         * a >>= 4;
         *
         * a = a | b;
         * a |= b;
         */
        System.out.println("\nBitwise Operator Compound Assignments");
        int ta = 1;
        int tb = 2;
        int tc = 3;

        ta |= 4;
        tb >>= 1;
        tc <<= 1;
        ta ^= tc;

        System.out.println("ta = " + ta);
        System.out.println("tb = " + tb);
        System.out.println("tc = " + tc);
    }
}
