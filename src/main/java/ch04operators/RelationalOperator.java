package ch04operators;

public class RelationalOperator {
    /**
     * The relational operator determine the relationship that one operand has to
     * the other
     * Specifically, they determine equality and ordering
     * The relational operators are shown here:
     *
     * ==       Equal to
     * !=       Not equal to
     * >        Greater than
     * <        Less than
     * >=       Greater than or equal to
     * <=       Less than or equal to
     *
     * The outcome of these operations is a boolean value
     * The relation operators are most frequently used in the expressions that control the
     * if statement and the various loop statements
     */

    public static void main(String[] args) {
        int a = 4;
        int b = 1;
        boolean c = a < b;

        int done = 1;

        if(done == 0) {

        }

        if (done != 0) {

        }
    }
}
