package ch04operators;

public class ArithmeticOperators {

    /**
     * Arithmetic operators are used in mathematical expressions in the same way
     * that they are used in algebra
     *
     * +    Addition
     * -    Subtraction
     * *    Multiplication
     * /    Division
     * %    Modulus
     * ++   Increment
     * +=   Addition assignment
     * -=   Subtraction assignment
     * *=   Multiplication assignment
     * /=   Division assignment
     * %=   Modulus assignment
     * --   Decrement
     *
     * The operands must be of a numeric type
     */
    public static void main(String[] args) {
        // arithmetic using integers
        System.out.println("Integer Arithmetic");
        int a = 1 + 1;
        int b = a * 3;
        int c = b / 4;
        int d = c - a;
        int e = -d;
        System.out.println("a: " + a);
        System.out.println("b: " + b);
        System.out.println("c: " + c);
        System.out.println("d: " + d);
        System.out.println("e: " + e);

        // arithmetic using double
        System.out.println("\nFloating Point Arithmetic");
        double f = 1 + 1;
        double g = f * 3;
        double h = g / 4;
        double i = h - f;
        double j = -i;
        System.out.println();
        System.out.println("f: " + f);
        System.out.println("g: " + g);
        System.out.println("h: " + h);
        System.out.println("i: " + i);
        System.out.println("j: " + j);

        /**
         * The Modulus Operator
         *
         * The modulus operator % returns the remainder of a division operation
         */
        System.out.println("\nModulus Operator");
        int x = 42;
        double y = 42.25;

        System.out.println("x mod 10 = " + x % 10);
        System.out.println("y mod 10 = " + y % 10);

        /**
         * Arithmetic Compound Assignment Operator
         *
         * var = var op expression;
         * var op= expression;
         */
        System.out.println("\nArithmetic Compound Assignment Operator");
        int ma = 1;
        int mb = 2;
        int mc = 3;

        ma += 5;
        mb *= 4;
        mc += ma * mb;
        mc %= 6;
        System.out.println("ma = " + ma);
        System.out.println("mb = " + mb);
        System.out.println("mc = " + mc);

        /**
         * Increment and Decrement
         */
        System.out.println("\nIncrement and Decrement");
        int ia = 1;
        int ib = 2;
        int ic;
        int id;

        ic = ++ib;
        id= ia++;
        ic++;

        System.out.println("ia = " + ia);
        System.out.println("ib = "+ ib);
        System.out.println("ic = " + ic);
        System.out.println("id = "+ id);
    }
}
