package ch04operators;

public class BooleanLogicalOperators {

    /**
     * The Boolean logical operators shown here operate only on boolean operands
     * All of the binary logical operators combine two boolean values to form
     * a resultant boolean value
     *
     * &        Logical AND
     * |        Logical OR
     * ^        Logical XOR (exclusive OR)
     * ||       Short-circuit OR
     * &&       Short-circuit AND
     * !        Logical unary NOT
     * &=       AND assignment
     * |=       OR assignment
     * ^=       XOR assignment
     * ==       Equal to
     * !=       Not equal to
     * ?:       Ternary if-then-else
     */
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        boolean c = a | b;
        boolean d = a & b;
        boolean e = a ^ b;
        boolean f = (!a & b) | (a & !b);
        boolean g = !a;

        System.out.println("        a = " + a);
        System.out.println("        b = " + a);
        System.out.println("      a|b = " + a);
        System.out.println("      a&b = " + a);
        System.out.println("      a^b = " + a);
        System.out.println("!a&b|a&!b = " + a);
        System.out.println("       !a = " + a);

        /**
         * Short-Circuit Logical Operator
         *
         * if (denom ! = 0 && num / denom > 10)
         * if (c == 1 & e++ < 100) d = 100;
         */

        /**
         * The Assignment Operator
         *
         * var = expression;
         */
        int x, y, z;
        x = y = z = 100; // set x, y, and z to 100

        /**
         * The ? operator
         *
         * expression ? expression2 : expression3
         *
         * ratio = denom == 0 ? 0 : num / denom;
         */

        int i, k;

        i = 10;
        k = i < 0 ? -i : i; // get absolute value of i
        System.out.print("Absolute value of ");
        System.out.println(i + " is " + k);

        i = -10;
        k = i < 0 ? -i : i;
        System.out.print("Absolute value of ");
        System.out.println(i + " is " + k);
    }
}
