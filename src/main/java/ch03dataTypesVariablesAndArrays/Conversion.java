package ch03dataTypesVariablesAndArrays;

/**
 * Demonstrate cast
 */
public class Conversion {

    public static void main(String[] args) {
        byte b;
        int i = 257;
        double d = 323.142;

        System.out.println("Conversion of int to byte");
        b = (byte) i;
        System.out.println("i and b " + i + " " + b);

        System.out.println("Conversion of double to int");
        i = (int) d;
        System.out.println("d and i " + d + " " +i);

        System.out.println("Conversion of double to byte");
        b = (byte) d;
        System.out.println("d and b "+ d + " " +b);

        /**
         * Automatic Type Promotion in Expressions
         *
         * Java automatically promotes each byte, short or char operand to int
         * when evaluating an expression
         */
        byte a1 = 40;
        byte b1 = 50;
        byte c1 = 100;
        int d1 = a1 * b1 / c1;

        byte b2 = 50;
        b2 = (byte) (b * 2);

    }
}
