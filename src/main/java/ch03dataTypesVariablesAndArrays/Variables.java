package ch03dataTypesVariablesAndArrays;

public class Variables {

    /**
     * The variable is the basic unit of storage in Java program
     * A variables is defined by the combination of an identifier,
     * a type, and an optional initializer.
     * In addition, all variables have a scope, which defines their visibility
     * and lifetime
     *
     * type identifier [ = value][, identifier [= value ]...];
     */

    int a, b, c;            // declares three ints, a, b, and c
    int d = 3, e, f = 5;    // declares three more ints, initializing d and f
    byte z = 32;            // initializes z
    double pi = 3.14159;    // declares an approximation of pi
    char x = 'x';           // the variable x has the value 'x'

    public static void dynamicInitialization() {
        double a = 3.0, b = 4.0;

        // c is dynamically initialized
        double c = Math.sqrt(a * a + b * b);

        System.out.println("Hypotenuse is " + c);

        int a1 = 9;
        byte b1;

        b1 = (byte) a1;
    }

    public static void main(String[] args) {

    }
}
