package ch03dataTypesVariablesAndArrays;

public class Arrays {

    /**
     * type var-name[];
     * array-var = new type[size];
     * type[] var-name;
     */
    int monthDays[];
    {
        monthDays = new int[12];
        monthDays[1] = 28;
    }

    public static void main(String[] args) {
        int monthDays[];
        monthDays = new int[12];
        monthDays[0] = 31;
        monthDays[1] = 28;
        monthDays[2] = 31;
        monthDays[3] = 30;
        monthDays[4] = 31;
        monthDays[5] = 30;
        monthDays[6] = 31;
        monthDays[7] = 31;
        monthDays[8] = 30;
        monthDays[9] = 31;
        monthDays[10] = 30;
        monthDays[11] = 31;

        System.out.println("April has " + monthDays[3] + " days");

        int monthDays2[] = { 1, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        System.out.println("April has " + monthDays2[3] + " days.");

        double nums[] = {10.1, 11.2, 12.3, 13.4, 14.5};
        double result = 0;
        int i;
        for (i = 0; i < 5; i++) {
            result = result + nums[i];
        }
        System.out.println("Averdage is " + result / 5);

        /**
         * Multidimensional arrays are implemented as arrays of arrays
         */
        int twoD[][] = new int[4][5];
        int j, k = 0;

        for(i = 0; i < 4; i++) {
            for(j = 0; j < 5; j++) {
                twoD[i][j] = k;
                k++;
            }
        }

        for(i = 0; i < 4; i++) {
            for(j = 0; j < 5; j++) {
                System.out.print(twoD[i][j] + " ");
            }
            System.out.println();
        }


        /**
         * When you allocate memory for a multidimensional array, you need only
         * sprecify the memory for the first (leftmost) dimension
         */
        int twoD2[][] = new int[4][];
        twoD2[0] = new int[5];
        twoD2[1] = new int[5];
        twoD2[2] = new int[5];
        twoD2[3] = new int[5];

        /**
         * Manually allocate differing size second dimesions
         */
        int twoD3[][] = new int[4][];
        twoD3[0] = new int[1];
        twoD3[1] = new int[2];
        twoD3[2] = new int[3];
        twoD3[3] = new int[4];

        k = 0;

        for(i = 0; i < 4; i++) {
            for(j = 0; j<i+1; j++) {
                twoD3[i][j] = k;
                k++;
            }
        }

        System.out.println();

        for(i = 0; i < 4; i++) {
            for(j = 0; j < i+1; j++) {
                System.out.print(twoD3[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();

        /**
         * Initialize a two-dimensional array
         */
        double m[][] = {
                { 0*0, 1*0, 2*0, 3*0 },
                { 0*1, 1*1, 2*1, 3*1 },
                { 0*2, 1*2, 2*2, 3*2 },
                { 0*3, 1*.3, 2*3, 3*3}
        };

        for(i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }

        /**
         * ThreeDMatrix
         */
        System.out.println();

        int threeD[][][] = new int[3][4][5];

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 4; j++) {
                for (k = 0; k < 5; k++) {
                    threeD[i][j][k] = i * j * k;
                }
            }
        }

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 4; j++) {
                for (k = 0; k < 5; k++) {
                    System.out.print(threeD[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

        /**
         * Alternative Array Declaration Syntax
         */
        int al[] = new int[3];
        int[] a2 = new int[3];

        char twod1[][] = new char[3][4];
        char[][] twod2 = new char[3][4];

        int[] nums1, nums2, nums3; // create three arrays
        int nums11[], nums22[], numms33[];

    }
}
