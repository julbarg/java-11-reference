package ch03dataTypesVariablesAndArrays;

public class Promote {

    /**
     * - All byte, short and char values are promoted to int
     * - If one operand is a long, the whole expression is promoted to long
     * - If one operand is a float, the entire expression is promoted to float
     * - If any of the operands are double, the result is a double
     */

    public static void main(String[] args) {
        byte b = 42;
        char c = 'a';
        short s = 1024;
        int i = 50000;
        float f = 5.6f;
        double d = .1234;

        double result = (f * b) + (i / c) - (f * s);
        System.out.println("result: " + result);
    }
}
