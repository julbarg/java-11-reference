package ch03dataTypesVariablesAndArrays;

/**
 * A simple demostration of local variable type inference
 */
public class VarDemo {

    public static void main(String[] args) {
        /**
         * Use type inference to determine the type of the
         * variable named avg
         * In this case, double is inferred
         */
        var avg = 10.0;
        System.out.println("Value of avg: " + avg);

        /**
         * In the following context, var is not a predefined identifier
         * It is simply a user-defined variable name
         */
        int var = 1;
        System.out.println("Value of var: "+ var);

        /**
         * Interestingly, in the following sequence, var is used as both
         * the type of the declaration and as a variable name
         * int the initializer
         */
        var k = -var;
        System.out.println("Value of k: " + k);

        var word = "Hola Mundo";
        System.out.println(word);

        var ints = Double.valueOf("89");

        var myArray = new int[10]; // This is valid
        /**
         * Notice that neither var nor myArray has brackets
         * Instead, the type of myArray is inferred t be int[]
         * Furthermore, you cannot use brackets on the left side of a var declaration
         * Thus, bot og these declarations are invalid
         */
        // var[] myArrays = new int[10]; // Wrong
        // var myArray[] = new int[10]; // Wrong

        /**
         * It is important to emphasize that var can be used to declare a variable only
         * when that variable is initialized
         * For example the following statement is incorrect
         */
        // var counter; // Wrong! Initializer required

        /**
         * var can be used only to declare local variables
         * It cannot be used when declaring instance variables, parameters, or return types
         *
         * Use in
         * - Generic types
         * - try-with-resources statement
         * - for loop
         */

        /**
         * Some var restrictions
         *
         * - Only one variable can be declare at a time
         * - A variable cannot use null as initializer
         * - The variable being declared cannot be used by the initializer expression
         */

        var myArrayss = new int[7]; // This is valid
        // var myArrayy = { 1, 2,  3}; // Wrong
    }
}
