package ch03dataTypesVariablesAndArrays;

public class PrimitiveTypes {

    /**
     * Java defines eight primitive types of data:
     * - byte
     * - short
     * - int
     * - long
     * - char
     * - float
     * - double
     * - boolean
     *
     * Integers This group includes byte, short, int, and long, which are for
     *      whole-valued signed numbers.
     * Floating-point numbers This group includes float and double, which
     *      represent numbers with fractional precision.
     * Characters This group includes char, which represents symbols in a
     *      character set, like letters and numbers.
     * Boolean This group includes boolean, which is a special type for
     *      representing true/false values.
     */

    /**
     * Integers
     * - long 64
     * - int 32
     * - short 16
     * - byte 8
     */
    byte smallerByte = -128;
    byte biggerByte = 127;

    short smallerShort = -32768;
    short biggerShort = 32767;

    int smallerInt = -2_147_483_648;
    int biggerInt = 2_147_483_647;

    long smallerLong = -9_223_372_036_854_775_808L;
    long biggerLong = 9_223_372_036_854_775_807L;

    /**
     * Floating-Point Types
     * - double 32 - 4.9e-324 to 1.8e+308 - single-precision
     * - float 64 - 1.4e-045 to 3.4e+038 - double-precision
     */

    float hightemp, lowtemp;

    public static double calculateAreaOfCircle(double r) {
        double pi = 3.1416;
        double area = pi * r * r;

        return area;
    }

    /**
     * Characters
     * Java uses Unicode to represents character
     * Unicode defines a fully international character set that can represents all
     * of the characters found in all human languages
     *
     * - char 16 bits - 0-65.536
     */

    /**
     * Integers Literals
     */
    public static void integersLiterals() {
        int x = 0b1010;
        System.out.println("x is " + x);

        int y = 123_456_789;
        int z = 123___456___789;

        int a = 0b1101_0101_0001_1010;
        System.out.println("a is " + a);

        long l = 0x7ffffffffffffffL;
        System.out.println("l is " + l);
    }

    public static void floatingPointLiterals() {
        /**
         * double - 64 bits
         * float - 32 bits
         */
        double x = 2.0;
        double y = 3.1416;
        double z = 0.6667;

        double a = 6.022E23;
        double b = 314159E-05;
        double c = 2e+100;

        double d = 3.14D;
        double d1 = 3.14d;
        float f = 3.14F;
        float f1 = 3.4f;

        System.out.println(x);
        System.out.println(y);
        System.out.println(z);

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        /**
         * Hexadecimal floating-point
         */
        double hx = 0x12.2P2;
        double hx1 = 0x12.2p2;

        double num = 9_423_497_862.0;
        num = 9_423_497.1_0_9;
    }

    public static void characterLiterals() {
        char a = 'a';
        char z = 'z';
        char ar = '@';

        /**
         * Especial characters
         */
        char singleQuote = '\'';
        System.out.println(singleQuote);

        /**
         * Octal Notation
         * Use the backslash followed by the three-digit number
         */
        char letterA = '\141';
        System.out.println(letterA);

        /**
         * Hexadecimal Notation
         * Enter a backslash-u
         */
        letterA = '\u0061';
        System.out.println(letterA);

        char japaneseKatakana = '\ua432';
        System.out.println(japaneseKatakana);

        char singleQuoteS = '\'';
        char doubleQuote = '\"';
        char backSlash = '\\';
        char carriageReturn = '\r';
        char newLine = '\n';
        char formFeed = '\f';
        char tab = '\t';
        char backspace = '\b';

        System.out.println(singleQuoteS);
        System.out.println(doubleQuote);
        System.out.println(backSlash);
        System.out.println(carriageReturn);
        System.out.println(newLine);
        System.out.println(formFeed);
        System.out.println(tab + "m");
        System.out.println(backspace + "m");
    }

    public static void stringLiterals() {
        /**
         * In some other languages string are implemented as arrays of characters
         * However, this is not the case in Java
         * Strings are actually object types
         * Because Java implements string s as objects, Java includes extensive
         * string-handling capabilities that are both powerful and easy to use
         */
        String hello = "Hello word";
        String twoLines = "two\nlines";
        String quotes = "\"This is in quotes\"";

        System.out.println(hello);
        System.out.println(twoLines);
        System.out.println(quotes);
    }


    public static void main(String[] args) {
        integersLiterals();
        floatingPointLiterals();
        characterLiterals();
        stringLiterals();
    }


}
