package ch02overview;

/**
 * Demonstrate the for loop
 */
public class ForTest {

    public static void main(String[] args) {
        // for(initialization; condition; iteration) statement;

        int x;

        for(x = 0; x < 10; x++) {
            System.out.println("This is x: " + x);
        }
    }
}
