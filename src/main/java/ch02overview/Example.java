package ch02overview;

/**
 * This is a simple Java program
 * Call this file Example.java
 */

/**
 * Compiling the Program
 *
 * C:\>javac Example.java
 * The javac compiler creates a file called Example.class
 *
 * Run
 * C:\>java Example
 */
public class Example {

    // Your program begins with a call to main()
    public static void main(String[] args) {
        System.out.println("This is a simple Java program");
    }
}
